import React, { createContext, useState } from "react";

export const ctx = createContext();
export default function CtxData(props) {
  const [listInfo, setListInfo] = useState([]);
  const [listDay, setDay] = useState({});
  const [listDetail, setListDetail] = useState({});
  return (
    <ctx.Provider
      value={{
        listInfo,
        setListInfo,
        listDay,
        setDay,
        listDetail,
        setListDetail,
      }}
    >
      {props.children}
    </ctx.Provider>
  );
}
