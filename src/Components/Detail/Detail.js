import React, { useContext } from "react";
import "./Detail.css";
import { ctx } from "../../CtxData";

export default function Detail() {
  const context = useContext(ctx);
  return (
    <div style={{ flexGrow: 1 }}>
      <h4 className="color-salmon margin pb-4">{new Date(context.listDetail.dt * 1000).toLocaleString("en-US", { weekday: "long" })}</h4>
      <div className="d-flex margin" style={{ justifyContent: "space-between" }}>
        <div className="">
          <h5 className="pb-3" style={{ color: "salmon" }}>
            Sun and Moon
          </h5>
          <p>Sunrise: {new Date(context.listDetail.moonrise * 1000).toLocaleString("en-US", { hour: "2-digit", minute: "2-digit" })}</p>
          <p>Sunset: {new Date(context.listDetail.sunset * 1000).toLocaleString("en-US", { hour: "2-digit", minute: "2-digit" })}</p>
          <p>Moonrise: {new Date(context.listDetail.moonrise * 1000).toLocaleString("en-US", { hour: "2-digit", minute: "2-digit" })}</p>
          <p>Moonset: {new Date(context.listDetail.moonset * 1000).toLocaleString("en-US", { hour: "2-digit", minute: "2-digit" })}</p>
        </div>
        <div className="">
          <h5 className="pb-3" style={{ color: "salmon" }}>
            Temperature
          </h5>
          <p>Day: {Math.round(context.listDetail.temp.day)}°C</p>
          <p>Min: {Math.round(context.listDetail.temp.min)}°C</p>
          <p>Max: {Math.round(context.listDetail.temp.max)}°C</p>
          <p>Night: {Math.round(context.listDetail.temp.night)}°C</p>
        </div>
        <div className="">
          <h5 className="pb-3" style={{ color: "salmon" }}>
            Feels like
          </h5>
          <p>Day: {Math.round(context.listDetail.feels_like.day)}°C</p>
          <p>Night: {Math.round(context.listDetail.feels_like.night)}°C</p>
          <p>Evening: {Math.round(context.listDetail.feels_like.eve)}°C</p>
          <p>Morning: {Math.round(context.listDetail.feels_like.morn)}°C</p>
        </div>
        <div className="">
          <h5 className="pb-3" style={{ color: "salmon" }}>
            Others
          </h5>
          <p>Wind degrees: {context.listDetail.wind_deg}°</p>
          <p>Wind speed: {context.listDetail.wind_speed}m/s</p>
          <p>Cloud: {context.listDetail.clouds}%</p>
          <p>UV: {context.listDetail.uvi}</p>
        </div>
      </div>
    </div>
  );
}
