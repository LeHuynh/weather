import React, { useContext } from "react";
import "./Home.css";
import { ctx } from "../../CtxData";
import InfoDay from "../InfoDay/InfoDay";

export default function Home() {
  const context = useContext(ctx);
  const handleKeyUp = (e) => {
    let value = e.target.value;
    if (e.keyCode === "13" || e.code === "Enter") {
      fetch(`https://openweathermap.org/data/2.5/find?q=${value}&appid=439d4b804bc8187953eb36d2a8c26a02`)
        .then((res) => res.json())
        .then((dt) => {
          console.log(dt);
          context.setListInfo(dt.list);
        });
    }
  };
  const handleClick = (lat, lon, name) => {
    context.setListDetail({});
    fetch(`https://openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=439d4b804bc8187953eb36d2a8c26a02`)
      .then((res) => res.json())
      .then((dt) => {
        console.log(dt);
        context.setDay({ name, dt });
      });
  };
  return (
    <div className="d-flex">
      <div className="bg-color">
        <input onKeyUp={handleKeyUp} placeholder="Search..." className="width border-radius margin padding border" />
        <p className="color-white text-align-right">Enter to Search...</p>
        {context.listInfo.map((el, index) => {
          return (
            <div onClick={() => handleClick(el.coord.lat, el.coord.lon, el.name)} key={index} className="mt-5 bg-white padding margin border-radius">
              <h4 className="d-flex">
                <img className="width-icon" width={"100%"} src={`https://openweathermap.org/images/flags/${el.sys.country.toLowerCase()}.png`} alt="error" />
                <span>{el.name}</span>
              </h4>

              <div>
                <p>
                  <span className="bg-black color-white border-radius-2 padding-2">{Math.round(el.main.temp * 1 - 273)}°C</span> temperature from{" "}
                  {Math.round(el.main.temp_min * 1 - 273)}°C to {Math.round(el.main.temp_max * 1 - 273)}°C wind {""}
                  {el.wind.speed} m/s. Clouds {""}
                  {el.clouds.all}%
                  <br />
                  <span>
                    Geo coords[{Math.round(el.coord.lat)}, {Math.round(el.coord.lon)}]
                  </span>
                </p>
              </div>
            </div>
          );
        })}
      </div>
      <div className="bg-color-1 padding-2">{context.listDay?.dt && context.listInfo && <InfoDay />}</div>
    </div>
  );
}
