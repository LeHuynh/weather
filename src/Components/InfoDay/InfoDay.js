import React, { useContext, useState } from "react";
import { ctx } from "../../CtxData";
import "./InfoDay.css";
import Detail from "../Detail/Detail";

export default function InfoDay() {
  const context = useContext(ctx);
  const [date, setDate] = useState(new Date());
  setInterval(() => setDate(new Date()), 1000);
  const handleClick = (element) => {
    context.setListDetail(element);
  };
  return (
    <div>
      <h2 className="color-salmon">
        {date.toLocaleTimeString("en-US", {
          hour: "2-digit",
          minute: "2-digit",
        })}
      </h2>
      <p>
        {date.toLocaleDateString("en-US", {
          dateStyle: "full",
        })}
      </p>
      <h1>Welcome to {context.listDay.name}.</h1>
      <div className="d-flex flex-wrap margin-auto color-white text-center">
        {context.listDay?.dt?.daily?.map((element, index2) => {
          return (
            <div onClick={() => handleClick(element)} key={index2} className="bg-salmon m-3 padding-box border-radius box-shadow">
              <h4>
                {element.weather?.map((item) => {
                  return <img key={item} src={`https://openweathermap.org/img/wn/${item.icon}@2x.png`} />;
                })}
              </h4>
              <h4>{new Date(element.dt * 1000).toLocaleString("en-US", { weekday: "short" })}</h4>
              <h4>{Math.round(element.temp.day)}C</h4>
            </div>
          );
        })}
      </div>
      <hr />
      <div className="d-flex">{context.listDetail.dt && <Detail />}</div>
    </div>
  );
}
