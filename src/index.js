import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import CtxData from "./CtxData";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <CtxData>
    <App />
  </CtxData>
);
